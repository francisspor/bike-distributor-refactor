# README #

The Bike Distributor Refactor for Trainer Road - Francis Spor (francis@upstatespors.com)

### My Notes and Ideas ###
I took a quick look through the code, and noticed that in Order.cs both Receipt() and HtmlReceipt() were calling a rather complex, but pretty much identical method to generate line item information.  

I first created a ToString method on Line.cs, so that a line item knows how to output itself.  Next, By doing that, I had to change the total calculation to a Linq.Sum call on the list of items, but that was pretty trivial.  So, once I'd DRY'd up the actual line generation into a single ToString method, I noticed that there was a lot of logic and duplicate code around creating the LineItem price.  

So, first I simplified that out to a calculation first of a line item total, and then ran it through the original case statement that calculated the volume discount.  Then, I was looking at that case statement, and realized, I could simplify that some by creating a struct that knows for the various price points, at what levels the quantity discounts occur.  And poof, that big case statement just changed to a lookup and simple if statement. 

### Original Challenge ###
Thanks for taking the time to download this refactoring exercise. 

Here's the instructions from our CTO:

This solution contains three classes used by an imaginary bicycle distributor to produce order receipts and some unit tests to prove that everything works.

Pretend this code is part of a larger software system and you are given responsibility for it. Assume that there will be at least one type of change coming your way regularly: more bikes at new prices, different discount codes and percentages, and/or additional receipt formats.

Refactor the code so that it can survive an onslaught of the changes you've chosen, you're confident it works, and you're comfortable the next engineer will easily understand how to work on it.

Show us what you can do; you should be proud of what you submit. If we love your refactoring and your resume is legit, we'll move to the next step and do an interview and pair programming session. This might seem like a lot of hoops to jump through, but it's way better than hiring someone who answers trivia questions in an interview well but can't code worth a damn.