﻿namespace BikeDistributor
{
    public class Line
    {
        public Line(Bike bike, int quantity)
        {
            Bike = bike;
            Quantity = quantity;
        }

        public Bike Bike { get; private set; }
        public int Quantity { get; private set; }

        public double LineTotal
        {
            get
            {
                double lineTotal = Quantity * Bike.Price;
                var pricingOptions = new BikePricingOptions()[Bike.Price];
                if (Quantity >= pricingOptions.Quantity)
                {
                    lineTotal = lineTotal * pricingOptions.Discount;
                }

                return lineTotal;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} x {1} {2} = {3}", Quantity, Bike.Brand, Bike.Model, LineTotal.ToString("C"));
        }
    }
}