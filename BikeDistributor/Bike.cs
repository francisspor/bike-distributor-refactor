﻿using System.Collections.Generic;

namespace BikeDistributor
{
    public class Bike
    {
        public Bike(string brand, string model, int price)
        {
            Brand = brand;
            Model = model;
            Price = price;
        }

        public string Brand { get; private set; }
        public string Model { get; private set; }
        public int Price { get; set; }
    }

    public class BikePricingOptions : Dictionary<int, BikePricingOption>
    {
        public const int OneThousand = 1000;
        public const int TwoThousand = 2000;
        public const int FiveThousand = 5000;

        public BikePricingOptions()
        {
            Add(OneThousand, new BikePricingOption {Discount = 0.9d, Quantity = 20});
            Add(TwoThousand, new BikePricingOption {Discount = 0.8d, Quantity = 10});
            Add(FiveThousand, new BikePricingOption {Discount = 0.8d, Quantity = 5});
        }
    }

    public class BikePricingOption
    {
        public int Quantity { get; set; }
        public double Discount { get; set; }
    }
}